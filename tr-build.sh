# Customize System Rescue CD and Build TechRescue
echo "Before proceeding, verify you have updated the settings.conf file with any necessary changes."
echo "Press Y to continue with build"

# Pull in settings from settings.conf
source settings.conf

# Clean up build environment
# If sysresccd-src exists, remove it.
sudo rm -rf sysresccd-src

# Clone a fresh sysrescue source:
git clone git@gitlab.com:fdupoux/sysresccd-src.git
sleep 2

# Clean up buildscript (line 9 is broke at time of writing)
sed -i 's/iso_version="$(<${version_file})"/iso_version="$(<"${version_file}")"/g' sysresccd-src/build.sh

sed -i  's/^arch=.*/c\arch="$(<"${arch_file}")"' sysresccd-src/build.sh

# Add techrescue custom repo to pacman.conf
sed -i "78,80s/^/#/" sysresccd-src/pacman.conf

# Replace 'packages' with custom TechRescue list
cat techrescue-packages > sysresccd-src/packages

# Change hostname to techrescue
sed -i "s/sysresccd/$host/g" sysresccd-src/airootfs/etc/hostname

# Copy over custom UVM wireless wifi service
cp -Rv usr/local sysresccd-src/airootfs/usr/

# Set credentials for UVM wireless connection based on settings.conf
sed -i "s/WIFI_USER/$wifi_user/g" sysresccd-src/airootfs/usr/local/bin/uvm-wifi
sed -i "s/WIFI_PASS/$wifi_pass/g" sysresccd-src/airootfs/usr/local/bin/uvm-wifi

# Initial root commands:
cat >> sysresccd-src/airootfs/root/customize_airootfs.sh <<\EOF
#
# TECH RESCUE MODIFICATIONS BELOW
#

# Systemctl modifications
systemctl enable NetworkManager
systemctl set-default graphical.target

# User / group modifications
groupadd sudo
useradd -m -G sudo -p $(openssl passwd -1 $localpass) $localuser

# Create custom mount points
mkdir /mnt/backup
mkdir /mnt/source
mkdir /mnt/windows
mkdir /mnt/macos

# Enable networking and start UVM wifi at boot
chmod +x /usr/local/bin/uvm-wifi
systemctl enable uvm-wifi.service
systemctl start uvm-wifi.service

#
# END OF TECH TEAM MODIFICATIONS
#

EOF

# Build the iso
cd sysresccd-src
sudo ./build.sh -v -N $filename -L $disklabel -P $pub -A $app_name
